<?php
if ( get_theme_mod( 'related_post' ) == 1 ) : 
							$tags = wp_get_post_tags($post->ID);
							if ($tags) {
								echo '<div class="related-post-heading"><i class="fa fa-share" style="font-size:14px"></i>';
								esc_html_e('  Related Posts: ','frindle'); ?>
										</div><br>
								<?php
									$first_tag = $tags[0]->term_id;
									$args=array(
										'tag__in' => array($first_tag),
										'post__not_in' => array($post->ID),
										'posts_per_page'=>4,
										'ignore_sticky_posts'=>1
									); 
									$my_query = new WP_Query($args);
	
										if( $my_query->have_posts() ) {
	
											while ($my_query->have_posts()) : $my_query->the_post(); ?>
												<div class="related-post-block">
													<div class="related-post-thumb">
														<?php if ( has_post_thumbnail() ): ?>
															<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('frindle-small-thumbnail'); ?> 
														<?php else: ?>
															<img src="<?php echo get_template_directory_uri(); ?>/Images/default-featured-image-thumb.png" height="96" width="161"> </img>
														<?php endif; ?>
													</div>
													<div class="related-post-title">
														<?php the_title(); ?></a> 
													</div>
												</div>
													
											<?php
											endwhile;
										}
										wp_reset_postdata();
							}

					endif;
?>